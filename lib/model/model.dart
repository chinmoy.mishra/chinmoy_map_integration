import 'package:flutter/material.dart';

class MapModel {
  MapModel(
    this.state,
    this.stateCode,
    this.color,
  );
  String state;
  String stateCode;
  Color color;
}
