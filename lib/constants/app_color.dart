import 'package:flutter/material.dart';

class AppColor {
  static final lightGreen = Colors.lightGreen[300];
  static final purple = Colors.purple[900];
  static final red = Colors.red[600];

  static final seeGreen = Colors.lightBlue[300];
  static final lightYello = Colors.yellow[400];
  static final purpleAccent = Colors.deepPurple[600];
  static final lightBlue = Colors.lightBlue[300];
  static final cyan = Color.fromARGB(255, 10, 218, 200);
  static final black = Colors.black;
  static final white = Colors.white;
  static final grey = Colors.grey[700];
}
