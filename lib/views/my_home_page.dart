import 'package:chinmoy_map_integration/constants/app_color.dart';
import 'package:chinmoy_map_integration/constants/app_string.dart';
import 'package:chinmoy_map_integration/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MapShapeSource? _mapSource;
  List<MapModel> data = <MapModel>[
    MapModel(AppString.newSouthWales, '       New\nSouth Wales',
        AppColor.lightYello!),
    MapModel(AppString.queensland, AppString.queensland, AppColor.cyan),
    MapModel(AppString.northernTerritory, 'Northern\nTerritory', AppColor.red!),
    MapModel(AppString.victoria, AppString.victoria, AppColor.purpleAccent!),
    MapModel(AppString.southAustralia, AppString.southAustralia,
        AppColor.lightBlue!),
    MapModel(AppString.westernAustralia, AppString.westernAustralia,
        AppColor.purple!),
    MapModel(AppString.tasmani, AppString.tasmani, AppColor.lightBlue!),
    MapModel(AppString.australianCapitalTerritory, 'ACT', AppColor.cyan),
  ];
  @override
  void initState() {
    data;
    _mapSource = MapShapeSource.asset(
      'assets/json/australia.json',
      shapeDataField: 'STATE_NAME',
      dataCount: data.length,
      primaryValueMapper: (int index) => data[index].state,
      shapeColorValueMapper: (int index) => data[index].color,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppString.appBerString),
        centerTitle: true,
        backgroundColor: AppColor.lightGreen,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SizedBox(
              height: double.infinity,
              child: SfMaps(layers: [
                MapShapeLayer(
                    source: _mapSource!,
                    zoomPanBehavior:
                        MapZoomPanBehavior(zoomLevel: double.negativeInfinity),
                    shapeTooltipBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(7),
                        child: Text(
                          data[index].state,
                          style: TextStyle(color: AppColor.white),
                        ),
                      );
                    },
                    tooltipSettings: MapTooltipSettings(
                        color: AppColor.grey,
                        strokeColor: AppColor.white,
                        strokeWidth: 2),
                    strokeColor: AppColor.white,
                    strokeWidth: 0.5,
                    dataLabelSettings: MapDataLabelSettings(
                        overflowMode: MapLabelOverflow.visible,
                        textStyle: TextStyle(
                          color: AppColor.white,
                          fontWeight: FontWeight.bold,
                        ))),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
